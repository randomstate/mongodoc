<?php


namespace RandomState\MongoDoc\Extensions;


use Doctrine\Common\EventManager;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Illuminate\Contracts\Container\Container;
use RandomState\MongoDoc\DocumentManagerRegistry;

class ExtensionManager {

	/**
	 * @var array
	 */
	protected $extensions = [];

	/**
	 * @var array
	 */
	protected $loaded = [];

	/**
	 * @var Container
	 */
	protected $container;

	public function __construct(Container $container)
	{
		$this->container = $container;
	}

	public function register($class)
	{
		$this->extensions[] = $class;
	}

	public function load(DocumentManagerRegistry $registry)
	{
		/** @var DocumentManager $manager */
		foreach($registry->getManagers() as $manager)
		{
			$this->loadExtensionsForManager($manager);
		}

	}

	public function loadExtensionsForManager(DocumentManager $manager)
	{
//		$connection = $manager->getConnection();

		foreach($this->extensions as $extension)
		{
			$extension = $this->container->make($extension);
			$this->loadExtension($extension, $manager, $manager->getEventManager(), $manager->getConfiguration());
		}
	}

	public function loadExtension(Extension $extension, DocumentManager $documentManager, EventManager $eventManager, Configuration $configuration)
	{
		if(! $this->isExtensionLoaded(get_class($extension)))
		{
			//add subscribers to the extension
			$extension->addSubscribers($eventManager, $documentManager, $configuration->getMetadataDriverImpl()->getReader());
			$this->loaded[] = $extension;
		}
	}

	public function isExtensionLoaded($class)
	{
		return isset($this->loaded[$class]);
	}
}