<?php


namespace RandomState\MongoDoc\Extensions;


use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\EventManager;
use Doctrine\ODM\MongoDB\DocumentManager;

interface Extension {

	public function addSubscribers(EventManager $eventManager, DocumentManager $documentManager, Reader $reader = null);

}