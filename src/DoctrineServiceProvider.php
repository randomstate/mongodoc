<?php


namespace RandomState\MongoDoc;


use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ODM\MongoDB\DocumentManager;
use Illuminate\Support\ServiceProvider;
use RandomState\MongoDoc\Extensions\ExtensionManager;
use RandomState\MongoDoc\Filters\FilterManager;

class DoctrineServiceProvider extends ServiceProvider {

	public function boot()
	{
		//Publish the configuration so that a user can override the defaults
		$this->publishes([
			$this->getConfigPath() => config_path('mongodoc.php'),
		], 'config');
	}

	public function register()
	{
		/*
		 * Get a list of connection parameters from the database for each connection (manager)
		 *      - database host
		 *      - database name
		 *      - database user
		 *      - database password
		 *      - database port
		 *
		 * Get a list of configuration parameters from the connection for each config
		 *      - proxy dir
		 *      - hydrator dir
		 *      - autogenerate proxies true/false
		 *      - autogenerate hydrators true/false
		 *      - namespace proxies/hydrators true/false
		 *
		 *      - custom DBAL types
		 *
		 * Create document managers for each connection, and bind them to the container
		 */

		$this->loadConfig();
		$this->bindDocumentManagerRegistry();
		$this->bindDefaultDocumentManager();
		$this->loadExtensions();
		$this->loadFilters();
	}

	private function loadConfig()
	{
		$this->mergeConfigFrom($this->getConfigPath(), 'mongodoc');
	}
	private function bindDocumentManagerRegistry()
	{
		$this->app->singleton('registry', function ($app) {

			$registry = new DocumentManagerRegistry($app, $app->make(DocumentManagerFactory::class));

			// Add all managers into the registry
			foreach ($app->make('config')->get('mongodoc.managers', []) as $manager => $settings) {
				$registry->addManager($manager, $settings);
				$registry->addConnection($manager);
			}

			return $registry;
		});

		$this->app->alias('registry', ManagerRegistry::class);
		$this->app->alias('registry', DocumentManagerRegistry::class);
	}

	private function loadExtensions()
	{
		$extensionManager = new ExtensionManager($this->app);
		$this->app->singleton('mongodoc.extension_manager', $extensionManager);
		$this->app->alias('mongodoc.extension_manager', ExtensionManager::class);

		$extensionManager->load($this->app->make('registry'));
	}

	private function loadFilters()
	{
		$filterManager = new FilterManager($this->app);
		$this->app->singleton('mongodoc.filter_manager', $filterManager);
		$this->app->alias('mongodoc.filter_manager', FilterManager::class);

		$filterManager->load($this->app->make('registry'));
	}

	private function bindDefaultDocumentManager()
	{
		// Bind the default Entity Manager
		$this->app->singleton('dm', function ($app) {
			return $app->make('registry')->getManager();
		});

		$this->app->alias('dm', DocumentManager::class);
		$this->app->alias('dm', ObjectManager::class);
	}

	private function getConfigPath()
	{
		return __DIR__ . "/../config/mongodoc.php";
	}
}