<?php


namespace RandomState\MongoDoc\Filters;


use Doctrine\Common\EventManager;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Illuminate\Contracts\Container\Container;
use RandomState\MongoDoc\DocumentManagerRegistry;
use RandomState\MongoDoc\Filters\Filter;

class FilterManager {

	/**
	 * @var array
	 */
	protected $filters = [];

	/**
	 * @var array
	 */
	protected $loaded = [];

	/**
	 * @var Container
	 */
	protected $container;

	public function __construct(Container $container)
	{
		$this->container = $container;
	}

	public function register($class)
	{
		$this->filters[] = $class;
	}

	public function load(DocumentManagerRegistry $registry)
	{
		/** @var DocumentManager $manager */
		foreach($registry->getManagers() as $manager)
		{
			$this->loadFiltersForManager($manager);
		}

	}

	public function loadFiltersForManager(DocumentManager $manager)
	{
		foreach($this->filters as $filter)
		{
			$filter = $this->container->make($filter);
			$this->loadFilter($filter, $manager->getConfiguration());
		}
	}

	public function loadFilter(Filter $filter, Configuration $configuration)
	{
		if( ! $this->isFilterLoaded(get_class($filter)))
		{
			$configuration->addFilter($filter->getName(), $filter);
			$this->loaded[] = $filter;
		}
	}

	public function isFilterLoaded($class)
	{
		return isset($this->loaded[$class]);
	}
}