<?php


namespace RandomState\MongoDoc\Filters;


use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\Query\Filter\BsonFilter;

abstract class Filter extends BsonFilter {

	abstract function getName();
}