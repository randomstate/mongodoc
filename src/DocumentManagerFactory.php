<?php


namespace RandomState\MongoDoc;


use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use Illuminate\Container\Container;
use Illuminate\Contracts\Config\Repository;
use MongoDB\Driver\Exception\InvalidArgumentException;

class DocumentManagerFactory {

	/**
	 * @var Container
	 */
	private $container;

	/**
	 * @var Repository
	 */
	private $configuration;

	public function __construct(Container $container, Repository $config)
	{
		$this->container = $container;
		$this->configuration = $config;
	}

	public function make(array $settings)
	{
		$configuration = new Configuration();
		$connection = new Connection();

		$this->configureProxies($settings, $configuration);
		$this->configureHydrators($settings, $configuration);
		$this->configureMetadataDriverImplementation($settings, $configuration);
		
		$connectionDriver = $this->getConnectionDriver($settings);
		$configuration->setDefaultDB($connectionDriver['database']);

		return DocumentManager::create($connection, $configuration);
	}

	private function configureProxies(array $settings, Configuration $configuration)
	{
		$configuration->setProxyDir(
			array_get($settings, 'proxies.path')
		);
		$configuration->setAutoGenerateProxyClasses(
			array_get($settings, 'proxies.auto_generate', false)
		);

		if($namespace = array_get($settings, 'proxies.namespace', false))
		{
			$configuration->setProxyNamespace($namespace);
		}
	}

	private function configureHydrators(array $settings, Configuration $configuration)
	{
		$configuration->setHydratorDir(
			array_get($settings, 'hydrators.path')
		);
		$configuration->setAutoGenerateHydratorClasses(
			array_get($settings, 'hydrators.auto_generate', false)
		);

		if($namespace = array_get($settings, 'hydrators.namespace', false))
		{
			$configuration->setHydratorNamespace($namespace);
		}
	}
	
	private function configureMetadataDriverImplementation(array $settings, Configuration $configuration)
	{
		$metadataDriverChain = new MappingDriverChain();

		$defaultDriver = AnnotationDriver::create(array_get($settings, 'paths'));

		$metadataDriverChain->addDriver($defaultDriver, "MongoDoc");
		$metadataDriverChain->setDefaultDriver($defaultDriver);

		AnnotationDriver::registerAnnotationClasses();

		$configuration->setMetadataDriverImpl($metadataDriverChain);
	}

	/**
	 * @param array $settings
	 *
	 * @return array
	 */
	private function getConnectionDriver(array $settings = [])
	{
		$connection = array_get($settings, 'connection');
		$key        = 'database.connections.' . $connection;

		if (!$this->configuration->has($key)) {
			throw new InvalidArgumentException("Connection [{$connection}] has no configuration in [{$key}]");
		}

		return $this->configuration->get($key);
	}
}