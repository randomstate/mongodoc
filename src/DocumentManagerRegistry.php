<?php


namespace RandomState\MongoDoc;


use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Proxy\Proxy;
use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\MongoDBException;
use Illuminate\Contracts\Container\Container;
use RandomState\MongoDoc\Exceptions\ManagerIsNotDefinedException;

class DocumentManagerRegistry implements ManagerRegistry {

	const CONNECTION_BINDING_PREFIX = "mongodoc.connections.";

	const MANAGER_BINDING_PREFIX = "mongodoc.managers.";

	/**
	 * @var Container
	 */
	protected $container;

	/**
	 * @var DocumentManagerFactory
	 */
	protected $factory;

	/**
	 * @var string
	 */
	protected $defaultManagerName = 'default';

	/**
	 * @var string
	 */
	protected $defaultConnectionName = 'default';

	/**
	 * @var Connection[]
	 */
	protected $connections = [];

	/**
	 * @var ObjectManager[]
	 */
	protected $managers = [];

	/**
	 * DocumentManagerRegistry constructor.
	 *
	 * @param Container $container
	 * @param DocumentManagerFactory $factory
	 */
	public function __construct(Container $container, DocumentManagerFactory $factory)
	{
		$this->container = $container;
		$this->factory = $factory;
	}

	/**
	 * @param       $name
	 * @param array $settings
	 */
	public function addManager($name, array $settings = [])
	{
		if (!$this->container->bound($this->getManagerBindingName($name))) {
			$this->container->singleton($this->getManagerBindingName($name), function () use ($settings) {
				return $this->factory->make($settings);
			});

			$this->managers[$name] = $this->container->make($this->getManagerBindingName($name));
		}
	}

	/**
	 * @param $name
	 */
	public function addConnection($name)
	{
		if (!$this->container->bound($this->getConnectionBindingName($name))) {
			$this->container->singleton($this->getConnectionBindingName($name), function () use ($name) {
				return $this->getManager($name)->getConnection();
			});

			$this->connections[$name] = $this->container->make($this->getConnectionBindingName($name));
		}
	}

	/**
	 * Gets the default connection name.
	 *
	 * @return string The default connection name.
	 */
	public function getDefaultConnectionName()
	{
		return $this->defaultConnectionName;
	}

	/**
	 * Gets the named connection.
	 *
	 * @param string $name The connection name (null for the default one).
	 *
	 * @return object
	 */
	public function getConnection($name = null)
	{
		$name ?: $this->getDefaultConnectionName();

		if(isset($this->connections[$name]))
		{
			return $this->connections[$name];
		}
	}

	/**
	 * Gets an array of all registered connections.
	 *
	 * @return array An array of Connection instances.
	 */
	public function getConnections()
	{
		return $this->connections;
	}

	/**
	 * Gets all connection names.
	 *
	 * @return array An array of connection names.
	 */
	public function getConnectionNames()
	{
		return array_keys($this->connections);
	}

	/**
	 * Gets the default object manager name.
	 *
	 * @return string The default object manager name.
	 */
	public function getDefaultManagerName()
	{
		return $this->defaultManagerName;
	}

	/**
	 * Gets a named object manager.
	 *
	 * @param string $name The object manager name (null for the default one).
	 *
	 * @throws ManagerIsNotDefinedException
	 * @return \Doctrine\Common\Persistence\ObjectManager
	 */
	public function getManager($name = null)
	{
		$name ?: $this->getDefaultManagerName();
		if(!$name)
		{
			$name = $this->getDefaultManagerName();
		}

		if(isset($this->managers[$name]))
		{
			return $this->managers[$name];
		}
		
		throw new ManagerIsNotDefinedException;
	}

	/**
	 * Gets an array of all registered object managers.
	 *
	 * @return \Doctrine\Common\Persistence\ObjectManager[] An array of ObjectManager instances
	 */
	public function getManagers()
	{
		return $this->managers;
	}

	/**
	 * Resets a named object manager.
	 *
	 * This method is useful when an object manager has been closed
	 * because of a rollbacked transaction AND when you think that
	 * it makes sense to get a new one to replace the closed one.
	 *
	 * Be warned that you will get a brand new object manager as
	 * the existing one is not useable anymore. This means that any
	 * other object with a dependency on this object manager will
	 * hold an obsolete reference. You can inject the registry instead
	 * to avoid this problem.
	 *
	 * @param string|null $name The object manager name (null for the default one).
	 *
	 * @throws ManagerIsNotDefinedException
	 * @return \Doctrine\Common\Persistence\ObjectManager
	 */
	public function resetManager($name = null)
	{
		$name = $name ?: $this->getDefaultManagerName();

		if( ! $this->managerExists($name))
		{
			throw new ManagerIsNotDefinedException(sprintf('Doctrine Manager named "%s" does not exist.', $name));
		}

		// force the creation of a new document manager
		// if the current one is closed
		$this->container->forgetInstance($this->getManagerBindingName($name));

		unset($this->managers[$name]);
	}

	/**
	 * Resolves a registered namespace alias to the full namespace.
	 *
	 * This method looks for the alias in all registered object managers.
	 *
	 * @param string $alias The alias.
	 *
	 * @throws MongoDBException
	 *
	 * @return string The full namespace.
	 */
	public function getAliasNamespace($alias)
	{
		foreach($this->getManagerNames() as $name)
		{
			try
			{
				return $this->getManager($name)->getConfiguration()->getDocumentNamespace($alias);
			} catch(MongoDBException $e)
			{
			}
		}

		throw MongoDBException::unknownDocumentNamespace($alias);
	}

	/**
	 * Gets all connection names.
	 *
	 * @return array An array of connection names.
	 */
	public function getManagerNames()
	{
		return array_keys($this->connections);
	}

	/**
	 * Gets the ObjectRepository for an persistent object.
	 *
	 * @param string $persistentObject The name of the persistent object.
	 * @param string $persistentManagerName The object manager name (null for the default one).
	 *
	 * @return \Doctrine\Common\Persistence\ObjectRepository
	 */
	public function getRepository($persistentObject, $persistentManagerName = null)
	{
		return $this->getManager($persistentManagerName)->getRepository($persistentObject);
	}

	/**
	 * Gets the object manager associated with a given class.
	 *
	 * @param string $class A persistent object class name.
	 *
	 * @return \Doctrine\Common\Persistence\ObjectManager|null
	 */
	public function getManagerForClass($class)
	{
		// Check for namespace alias
		if(strpos($class, ':') !== false)
		{
			list($namespaceAlias, $simpleClassName) = explode(':', $class, 2);
			$class = $this->getAliasNamespace($namespaceAlias) . '\\' . $simpleClassName;
		}

		$proxyClass = new \ReflectionClass($class);
		if($proxyClass->implementsInterface(Proxy::class))
		{
			$class = $proxyClass->getParentClass()->getName();
		}

		foreach($this->getManagerNames() as $name)
		{
			$manager = $this->getManager($name);

			if( ! $manager->getMetadataFactory()->isTransient($class))
			{
				foreach($manager->getMetadataFactory()->getAllMetadata() as $metadata)
				{
					if($metadata->getName() === $class)
					{
						return $manager;
					}
				}
			}
		}
	}

	/**
	 * @param $name
	 *
	 * @return bool
	 */
	protected function managerExists($name)
	{
		return isset($this->managers[$name]);
	}

	/**
	 * @param $name
	 *
	 * @return bool
	 */
	protected function connectionExists($name)
	{
		return isset($this->connections[$name]);
	}

	/**
	 * @param $manager
	 *
	 * @return string
	 */
	protected function getManagerBindingName($manager)
	{
		return self::MANAGER_BINDING_PREFIX . $manager;
	}

	/**
	 * @param $connection
	 *
	 * @return string
	 */
	protected function getConnectionBindingName($connection)
	{
		return self::CONNECTION_BINDING_PREFIX . $connection;
	}
}