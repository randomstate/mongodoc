<?php

use Doctrine\ODM\MongoDB\DocumentRepository;

return [
	/*
	 * Define your Document Managers below.
	 * They can only ever be MongoDB with this library, sorry!
	 */
	'default_manager'  => 'default',
	'managers'         => [
		'default' => [
			'dev'             => env('APP_DEBUG', false),
			'metadata_driver' => env('DOCTRINE_METADATA', 'annotations'),
			'connection'      => env('DB_CONNECTION', 'mongodb'),
			'paths'           => [
				base_path('app/Entities'),
			],
			'repository'      => DocumentRepository::class,
			'proxies'         => [
				'namespace'     => 'Proxies',
				'path'          => storage_path('proxies'),
				'auto_generate' => env('DOCTRINE_PROXY_AUTOGENERATE', false),
			],
			'hydrators'       => [
				'namespace'     => 'Hydrators',
				'path'          => storage_path('hydrators'),
				'auto_generate' => env('DOCTRINE_HYDRATOR_AUTOGENERATE', true),
			],
		],
	],
	'gedmo_extensions' => [
		'enabled'    => false,
		'extensions' => [

		],
	],
	'filters' => [

	]
];